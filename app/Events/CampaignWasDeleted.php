<?php

namespace App\Events;

use App\Campaign;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class CampaignWasDeleted extends Event
{
    use SerializesModels;

    public $campaign;

    /**
     * Create a new event instance.
     *
     * @param  Campaign  $campaign
     * @return void
     */
    public function __construct(Campaign $campaign)
    {
        $this->campaign = $campaign;
    }
}
