<?php

namespace App\Events;

//use App\Campaign;
use Illuminate\Database\Eloquent\Collection;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class CampaignsWereListed extends Event
{
    use SerializesModels;

    public $campaign;

    /**
     * Create a new event instance.
     *
     * @param  Collection  $campaign
     * @return void
     */
    public function __construct(Collection $campaigns)
    {
        $this->campaigns = $campaigns;
    }
}
