<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request,
    Illuminate\Http\Response,
    App\Http\Requests;

use Validator;

use App\Campaign,
    App\Asset;

use Event,
    App\Events\CampaignWasServed,
    App\Events\CampaignWasCreated,
    App\Events\CampaignWasUpdated,
    App\Events\CampaignWasDeleted,
    App\Events\CampaignsWereListed;

class CampaignController extends Controller
{
    private $videoTag = <<<EOD
<?xml version="1.0" encoding="UTF-8"?>
    <VAST version="2.0">
        <Ad id="preroll-1">
            <InLine>
                <AdSystem>2.0</AdSystem>
                <AdTitle>{CREATIVE_NAME}</AdTitle>
                <Creatives>
                    <Creative>
                    <Linear>
                    <Duration>00:00:30</Duration>
                    <MediaFiles>
                    <MediaFile height="396" width="600"
                    bitrate="496" type="video/x-flv"
                    delivery="progressive"><![CDATA[{VIDEO_URL}]]></MediaFile>
                    </MediaFiles>
                    </Linear>
                    </Creative>
                </Creatives>
            </InLine>
        </Ad>
    </VAST>
EOD;

    private $jsTag = <<<EOD
<script>var RNS = (new String (Math.random())).substring (2,11); document.write('<scr'+'ipt type="text/javascript" language="javascript1.1" src="https://adserver.xaxix.com/ad.js?asset=[CREATIVE_NAME]&cb='+RNS'"></scri'+'pt>');</script>
EOD;

    private $pixelTag = <<<EOD
<script src="https://adserver.xaxix.com/pixel.js?name=[PIXEL_NAME]"></script>
EOD;


    private $segmentTag = <<<EOD
<script src="https://adserver.xaxix.com/segment.js?cat=[SEGMENT_CATEGORY]"></script>
EOD;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $campaigns = Campaign::all();

        Event::fire(new CampaignsWereListed($campaigns));

        return response()->json([
            'success'   => 'true',
            'campaigns'  => $campaigns,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'              => 'required|max:255',
            'budget'            => 'required|max:255',
            'asset_name'        => 'alpha_dash|required|max:255',
            'asset_type'        => 'in:js,video|required|max:255',
            'video_url'         => 'required_if:asset_type,video|url|max:65000',
            'pixel_name'        => 'alpha_dash|unique:assets|max:255',
            'segment_category'  => 'alpha_dash|max:255',
        ]);

        if ($validator->fails()) {
            return response()
                ->json([
                    'success'   => 'false',
                    'errors'    => $validator->getMessageBag(),
                ], 400);
        }

        // Create The Campaign...
        $campaign = new Campaign;
        $campaign = $this->setCampaign($campaign, $request);


        // Create The Asset...
        $asset = new Asset;
        $asset = $this->setAsset($asset, $request);


        $campaign->save();
        $asset->campaign_id = $campaign->id;
        $asset->save();

        Event::fire(new CampaignWasCreated($campaign));

        return response()->json([
            'success'   => 'true',
            'campaign'  => $campaign,
            'asset'     => $asset,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $campaign = Campaign::find($id);

        if (!$campaign) {
            return response()
                ->json([
                    'success'   => 'false',
                    'error'    => "Campaign could not be found",
                ], 400);
        }

        $asset = Campaign::find($id)->asset;

        $content = $this->formatAsset($campaign, $asset);

        Event::fire(new CampaignWasServed($campaign));

        return response()->json([
            'success'   => 'true',
            'content'   => $content,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campaign = Campaign::find($id);
        $asset = Campaign::find($id)->asset;

        $validator = Validator::make($request->all(), [
            'name'              => 'max:255',
            'budget'            => 'max:255',
            'asset_name'        => 'alpha_dash|max:255',
            'asset_type'        => 'in:js,video|max:255',
            'video_url'         => 'required_if:asset_type,video|url|max:65000',
            'pixel_name'        => 'alpha_dash|unique:assets,pixel_name,' . $asset->id . '|max:255',
            'segment_category'  => 'alpha_dash|max:255',
        ]);

        if ($validator->fails()) {
            return response()
                ->json([
                    'success'   => 'false',
                    'errors'    => $validator->getMessageBag(),
                ], 400);
        }


        $campaign = $this->setCampaign($campaign, $request);
        $asset = $this->setAsset($asset, $request);

        $campaign->save();
        $asset->save();

        Event::fire(new CampaignWasUpdated($campaign));

        return response()->json([
            'success'   => 'true',
            'campaign'  => $campaign,
            'asset'     => $asset,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $campaign = Campaign::find($id);

        Campaign::destroy($id);

        Event::fire(new CampaignWasDeleted($campaign));

        return response()->json([
            'success'   => 'true',
        ]);
    }

    /**
     * Take a campaign and asset object, and return a formatted string
     *
     * @param Campaign $campaign
     * @param Asset $asset
     * @return string
     */
    private function formatAsset(Campaign $campaign, Asset $asset) {
        $content = '';
        switch ($asset->type) {
            case 'video':
                $tmpTag = str_replace('{CREATIVE_NAME}', $campaign->name, $this->videoTag);
                $tmpTag = str_replace('{VIDEO_URL}', $asset->video_url, $tmpTag);
                $content = $tmpTag;
                break;
            default:
                $tmpTag = str_replace('[CREATIVE_NAME]', $campaign->name, $this->jsTag);
                $content = $tmpTag;
                break;
        }

        if ($asset->pixel_name) {
            $tmpTag = str_replace('[PIXEL_NAME]', $asset->pixel_name, $this->pixelTag);
            $content .= $tmpTag;
        } else if ($asset->segment_category) {
            $tmpTag = str_replace('[SEGMENT_CATEGORY]', $asset->segment_category, $this->segmentTag);
            $content .= $tmpTag;
        }

        return $content;
    }

    /**
     * @param Campaign $campaign
     * @param Request $request
     * @return Campaign
     */
    private function setCampaign(Campaign $campaign, Request $request) {
        $campaign->name = $request->name;
        $campaign->budget = $request->budget;

        return $campaign;
    }

    /**
     * @param Asset $asset
     * @param Request $request
     * @return Asset
     */
    private function setAsset(Asset $asset, Request $request) {
        $asset->name = $request->asset_name;
        $asset->type = $request->asset_type;

        if ($asset->type == 'video') {
            if (!$request->video_url) {
                die("No video url passed");
            }
            $asset->video_url = $request->video_url;
        } else {
            $asset->video_url = null;
        }

        if ($request->pixel_name || $request->segment_category) {
            // set segment category if passed with no pixel name, otherwise only set pixel
            if ($request->segment_category && !$request->pixel_name) {
                $asset->segment_category = $request->segment_category;
                $asset->pixel_name = null;
            } else {
                $asset->pixel_name = $request->pixel_name;
                $asset->segment_category = null;
            }
        }

        return $asset;
    }
}
