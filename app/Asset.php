<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'type', 'video_url', 'pixel_name', 'segment_category', 'campaign_id',
    ];

    public function campaign() {
        return $this->belongsTo('App\Campaign');
    }
}
