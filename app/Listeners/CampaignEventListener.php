<?php

namespace App\Listeners;

use Log;

class CampaignEventListener
{
    /**
     * Handle campaign creation event.
     */
    public function onCampaignCreated($event) {
        Log::info("Campaign created", [
            'id'    => $event->campaign->id,
            'name'  => $event->campaign->name,
        ]);
    }

    /**
     * Handle serving campaign event.
     */
    public function onCampaignServed($event) {
        Log::info("Served campaign", [
            'id'    => $event->campaign->id,
            'name'  => $event->campaign->name,
        ]);
    }

    /**
     * Handle listing campaigns event.
     */
    public function onCampaignsListed($event) {
        Log::info("Displaying list of all campaigns");
    }

    /**
     * Handle deleting campaign events.
     */
    public function onCampaignDeleted($event) {
        Log::info("Campaign deleted", [
            'id'    => $event->campaign->id,
            'name'  => $event->campaign->name,
        ]);
    }

    /**
     * Handle serving campaign events.
     */
    public function onCampaignUpdated($event) {
        Log::info("Campaign Updated", [
            'id'    => $event->campaign->id,
            'name'  => $event->campaign->name,
        ]);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\CampaignWasCreated',
            'App\Listeners\CampaignEventListener@onCampaignCreated'
        );

        $events->listen(
            'App\Events\CampaignWasServed',
            'App\Listeners\CampaignEventListener@onCampaignServed'
        );

        $events->listen(
            'App\Events\CampaignsWereListed',
            'App\Listeners\CampaignEventListener@onCampaignsListed'
        );

        $events->listen(
            'App\Events\CampaignWasDeleted',
            'App\Listeners\CampaignEventListener@onCampaignDeleted'
        );

        $events->listen(
            'App\Events\CampaignWasUpdated',
            'App\Listeners\CampaignEventListener@onCampaignUpdated'
        );
    }

}