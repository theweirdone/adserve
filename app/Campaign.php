<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'budget'
    ];

    public function asset() {
        return $this->hasOne('App\Asset');
    }

    public function getBudgetAttribute($value) {
        return number_format($value, 2);
    }
}
