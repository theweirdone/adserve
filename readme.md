# Xaxis Test

## Available Commands

### Create Campaign (Post)
http://ads.dev/api/campaign

| Param            | Type   | Description      | Details           |
| ---------------- | ------ | ---------------- | ----------------- |
| name             | String | Campaign Name    | Required |
| budget           | Float  | Campaign Budget  | Required |
| asset_name       | String | Asset Name       | Required |
| asset_type       | String | Asset Type       | Required "js"/"video" |
| video_url        | String | Video URL        | Required if asset_type is "video" |
| pixel_name       | String | Pixel Name       | Optional |
| segment_category | String | Segment Category | Optional, ignored if pixel_name set |


### List Campaigns (Get)
http://ads.dev/api/campaign

### Serve Campaign (Get)
http://ads.dev/api/campaign/serve/{id}

### Edit Campaign {Put/Patch)
http://ads.dev/api/campaign/{id}

| Param            | Type   | Description      | Details           |
| ---------------- | ------ | ---------------- | ----------------- |
| name             | String | Campaign Name    | Required |
| budget           | Float  | Campaign Budget  | Required |
| asset_name       | String | Asset Name       | Required |
| asset_type       | String | Asset Type       | Required "js"/"video" |
| video_url        | String | Video URL        | Required if asset_type is "video" |
| pixel_name       | String | Pixel Name       | Optional |
| segment_category | String | Segment Category | Optional, ignored if pixel_name set |

### Delete Campaign {Delete}
http://ads.dev/api/campaign/{id}
